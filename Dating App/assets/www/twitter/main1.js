function checkConnection() {
    var networkState = navigator.network.connection.type;

    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.NONE]     = 'No network connection';

    if (states[networkState] === 'No network connection') {
        //alert('No network connection detected. Check settings.');
    } else {
        //alert('Connection type: ' + states[networkState]);
    }
}

function onDeviceReady() {
    document.addEventListener("offline", checkConnection, false);
    document.addEventListener("online", checkConnection, false);
}

function init() {
    document.addEventListener("deviceready", onDeviceReady, false);
}

$('#prdouctdetails').live('pageinit', function() {
    var oauth;
    var requestParams;
    var options = { 
    		  consumerKey: 'tc2fkAK7i6SWTAY56ABvw',
              consumerSecret: '2NEyyypyILsHpP8BafHNXZM3pQ3rMiihZ4PwOLc',
              callbackUrl: 'http://www.giftsms.com' };
    var mentionsId = 0;
    var localStoreKey = "flapp";
    //$('#stage-data').hide();
    //$('#stage-auth').hide();
  
    // Check for access token key/secret in localStorage
    var storedAccessData, rawData = localStorage.getItem(localStoreKey);
    if (rawData !== null) {
        storedAccessData = JSON.parse(rawData);                 
        options.accessTokenKey = storedAccessData.accessTokenKey;
        options.accessTokenSecret = storedAccessData.accessTokenSecret;
       
          
        console.log("AppLaudLog: Attemping oauth with stored token key/secret");           
        oauth = OAuth(options);
        oauth.get('https://api.twitter.com/1.1/account/verify_credentials.json?skip_status=true',
                function(data) {
                    var entry = JSON.parse(data.text);
                    console.log("AppLaudLog: Success getting credentials. screen_name: " + entry.screen_name);
                    //alert("USER");
                    /*    
                    $('#confirm-user').live('click', function() {
                        $('#oauthStatus').html('<span style="color:green;">Success!</span>');
                        $('#userInfo').html('Current user: <strong>' + entry.screen_name + '</strong>');
                        $('#stage-data').show();
                        $.mobile.changePage($('#page-home'), { reverse : true, changeHash: false });
                        return false;
                    });
                    $('#cancel-user').live('click', function() {
                        $('#cancel').trigger('click');
                        $.mobile.changePage($('#page-home'), { reverse : true, changeHash: false });
                        return false;
                    });

                    $('#dialog-confirm-text').html('<p>Twitter user: <strong>'
                         + entry.screen_name + '</strong><br> already authorized.<br>Continue as <strong>' 
                         + entry.screen_name + '</strong>?</p><p>Cancel to log in a different user.</p><hr>');
                    $('#stage-reading-local-store').hide();
                    $.mobile.changePage($('#page-dialog-confirm'), { role: 'dialog', changeHash: false });
                    */
                },
                function(data) { 
                    alert('Error with stored user data. Re-start authorization.');
                    options.accessTokenKey = '';
                    options.accessTokenSecret = '';
                    localStorage.removeItem(localStoreKey);
                    //$('#stage-reading-local-store').hide();
                    //$('#stage-auth').show();
                    console.log("AppLaudLog: No Authorization from localStorage data"); 
                }
        );
    } else {
        console.log("AppLaudLog: No localStorage data");
        $('#stage-reading-local-store').hide();
        $('#stage-auth').show();
    }
    /*
    function textCount() {
        var remaining = 140 - $('#tweettextarea').val().length;
        var color = (remaining < 0) ? 'red' : 'green';
        $('#textcount').html('<span style="color:' + color + ';">' + remaining + '</span> chars left. Enter text:');
    }
    textCount();
    */
    
    //$('#tweettextarea').change(textCount);
    //$('#tweettextarea').keyup(textCount);
    
    $('#share-twitter_btn').click(function() {
    	var productName =window.localStorage.getItem("prodName");
    	var prodDescription =window.localStorage.getItem("prodDescription");
    	 var message ="Check out this cool "+" product:"+productName+"\n"+ prodDescription+"  on their app for iPhone and Android!";
    	var twitterPost =message;
        // Set childBrowser callback to detect our oauth_callback_url
        if (typeof window.plugins.childBrowser.onLocationChange === "function") {
            window.plugins.childBrowser.onLocationChange = function(loc){
                console.log("0!!!AppLaudLog: onLocationChange : " + loc);
  
                // If user hit "No, thanks" when asked to authorize access
                if (loc.indexOf("http://www.giftsms.com?denied") >= 0) {
                    //$('#oauthStatus').html('<span style="color:red;">1User declined access</span>');
                    window.plugins.childBrowser.close();
                    return;
                }

                // Same as above, but user went to app's homepage instead
                // of back to app. Don't close the browser in this case.
                if (loc === "http://www.giftsms.com") {
                    //$('#oauthStatus').html('<span style="color:red;">2User declined access</span>');
                	 window.plugins.childBrowser.close();
                    return;
                }
                
                // The supplied oauth_callback_url for this session is being loaded
                if (loc.indexOf("http://www.giftsms.com?") >= 0) {
                    //alert(rawData);
                    var index, verifier = '';            
                    var params = loc.substr(loc.indexOf('?') + 1);
                    
                    params = params.split('&');
                    for (var i = 0; i < params.length; i++) {
                        var y = params[i].split('=');
                        if(y[0] === 'oauth_verifier') {
                            verifier = y[1];
                        }
                    }
               
                    // Exchange request token for access token
                    oauth.get('https://api.twitter.com/oauth/access_token?oauth_verifier='+verifier+'&'+requestParams,
                            function(data) {               
                                var accessParams = {};
                                var qvars_tmp = data.text.split('&');
                                for (var i = 0; i < qvars_tmp.length; i++) {
                                    var y = qvars_tmp[i].split('=');
                                    accessParams[y[0]] = decodeURIComponent(y[1]);
                                }
                                console.log('AppLaudLog: ' + accessParams.oauth_token + ' : ' + accessParams.oauth_token_secret);
                                //$('#oauthStatus').html('<span style="color:green;">Success!</span>');
                                //$('#stage-auth').hide();
                                //$('#stage-data').show();
                                oauth.setAccessToken([accessParams.oauth_token, accessParams.oauth_token_secret]);
                                
                                // Save access token/key in localStorage
                                var accessData = {};
                                accessData.accessTokenKey = accessParams.oauth_token;
                                accessData.accessTokenSecret = accessParams.oauth_token_secret;
                                console.log("AppLaudLog: Storing token key/secret in localStorage");
                                localStorage.setItem(localStoreKey, JSON.stringify(accessData));

                                oauth.get('https://api.twitter.com/1.1/account/verify_credentials.json?skip_status=true',
                                        function(data) {
                                            var entry = JSON.parse(data.text);
                                            //$('#userInfo').html('Current user: <strong>' + entry.screen_name + '</strong>');
                                            console.log("AppLaudLog: screen_name: " + entry.screen_name);
                                        },
                                        function(data) { 
                                           // alert('Error getting user credentials'); 
                                            console.log("AppLaudLog: Error " + data); 
                                            //$('#oauthStatus').html('<span style="color:red;">Error getting user credentials</span>');
                                        }
                                );                                         
                                window.plugins.childBrowser.close();
                                myTweet("Check out this cool Android app now available in the  google play store !"+twitterPost + " Check the app out and save.");
                        },
                        function(data) { 
                            alert('Error : No Authorization2'); 
                            console.log("AppLaudLog: 1 Error " + data); 
                            //$('#oauthStatus').html('<span style="color:red;">Error during authorization</span>');
                        }
                    );
                    //myTweet(); //tweeting is here
                }
               
            };  
        } // end if
        
        // Note: Consumer Key/Secret and callback url always the same for this app.        
        //$('#oauthStatus').html('<span style="color:blue;">Getting authorization...</span>');
        oauth = OAuth(options);
        oauth.get('https://api.twitter.com/oauth/request_token',
                function(data) {
                    requestParams = data.text; 
                    console.log("AppLaudLog: requestParams: " + data.text);
                    window.plugins.childBrowser.showWebPage('https://api.twitter.com/oauth/authorize?'+data.text, 
                            { showLocationBar : false });                    
                },
                function(data) { 
                    alert('Error : No Authorization'  + data);
                    console.log("AppLaudLog: 2 Error " + data); 
                    $('#oauthStatus').html('<span style="color:red;">Error during authorization</span>');
                }
        );
        mentionsId = 0;
    
    }); //twitter-button
    
    

    
    
    //$('#tweet').click(function() {   
    function myTweet(tweetMsg) {
    	
        var theTweet = tweetMsg;
        //alert(tweetMsg);
        
            oauth.post('https://api.twitter.com/1.1/statuses/update.json',
                    { 'status' : theTweet,  // jsOAuth encodes for us
                      'trim_user' : 'true' },
                    function(data) {
                        var entry = JSON.parse(data.text);
                        var data_html = '<h4>You Tweeted:</h4>';
                            
                        console.log("AppLaudLog: Tweet id: " + entry.id_str + " text: " + entry.text);
                        data_html = data_html.concat('<p>Id: ' + entry.id_str + '<br>Text: ' 
                                + entry.text + '</p>');
                        //$('#twitterdata').prepend(data_html);
                        //$('#tweettextarea').empty();
                        //$.mobile.changePage($('#page-home'), { reverse : true, changeHash: false });
                        alert("Tweeting success");
                    },
                    function(data) { 
                       // alert('Error Tweeting.'); 
                        console.log("AppLaudLog: Error during tweet " + data.text);
                        //$('#oauthStatus').html('<span style="color:red;">Error Tweeting</span>');                           
                        //$.mobile.changePage($('#page-home'), { reverse : true, changeHash: false });
                        //alert("page-home-error");
                    }
            );                  
    }
    //}); //click
    
    
    /*
    $('#networkbutton').click(function() {
        checkConnection();
    });
    */
});
