/*
var onDeviceReady = function() {
	console.log("deviceready event fired");
	// api-device
	// ***IMPORTANT: access device object only AFTER "deviceready" event
	document.getElementById("name").innerHTML = device.name;
	document.getElementById("pgversion").innerHTML = device.cordova ? device.cordova
			: device.phonegap;
	document.getElementById("platform").innerHTML = device.platform;
	document.getElementById("uuid").innerHTML = device.uuid;
	document.getElementById("version").innerHTML = device.version;
	// screen information ***Not necessary to wait for deviceready event
	document.getElementById("width").innerHTML = screen.width;
	document.getElementById("height").innerHTML = screen.height;
	document.getElementById("availwidth").innerHTML = screen.availWidth;
	document.getElementById("availheight").innerHTML = screen.availHeight;
	document.getElementById("colorDepth").innerHTML = screen.colorDepth;

	// api-events - see events.js for handler implementations
	// ***IMPORTANT: add event listeners only AFTER "deviceready" event
	document.addEventListener("searchbutton", onSearchKeyDown, false);
	document.addEventListener("menubutton", onMenuButtonDown, false);
	document.addEventListener("pause", onEventFired, false);
	document.addEventListener("resume", onEventFired, false);
	document.addEventListener("online", onEventFired, false);
	document.addEventListener("offline", onEventFired, false);
	// using callback for backbutton event may interfere with normal behavior
	document.addEventListener("backbutton", onBackbutton, false);
	document.addEventListener("batterycritical", onEventFired, false);
	document.addEventListener("batterylow", onEventFired, false);
	document.addEventListener("batterystatus", onEventFired, false);
	document.addEventListener("startcallbutton", onEventFired, false);
	document.addEventListener("endcallbutton", onEventFired, false);
	document.addEventListener("volumedownbutton", onEventFired, false);
	document.addEventListener("volumeupbutton", onEventFired, false);

	// api-camera Photo URI
	pictureSource = navigator.camera.PictureSourceType;
	destinationType = navigator.camera.DestinationType;

	// The Samsung Galaxy Tab 10.1 is currently the only device known to
	// support orientation/change correctly and reliably.
	if (device.name === "GT-P7510") {
		var updateScreen = function() {
			document.getElementById("width").innerHTML = screen.width;
			document.getElementById("height").innerHTML = screen.height;
			document.getElementById("availwidth").innerHTML = screen.availWidth;
			document.getElementById("availheight").innerHTML = screen.availHeight;
		};
		window.addEventListener("orientationchange", function(e) {
			// console.log("window.orientation: " + window.orientation);
			updateScreen();
		}, false);
	}
};
*/
var service = {
		
	sendRequest : function(urlVal, result) {
		var _url = urlVal;
		console.log("req>>" + _url);
		var retval;
		jQuery.ajax({
			url : _url,
			dataType : 'json',
			type : "POST",
			accepts : "application/json",
			beforeSend : function(x) {
				x.setRequestHeader("Content-Type", "application/json");
			},
			success : function(data, textStatus, jqXHR) {
				// Calls Success. If data found on the service then it would be
				// inside "DATA" variable
				console.log("success>>" + JSON.stringify(data));
				return result(data);// ALERT the data variable
				// retval=JSON.stringify(data);
			},
			error : function(xhr, error, code) {
				// SOMETHING WRONG WITH YOUR CALL.
				console.log("error>>" + JSON.stringify(xhr));
				// alert(JSON.stringify(xhr));
				return result(xhr);
				// retval=JSON.stringify(xhr);
			},
			complete : function() {
				// alert("Process Completed.");
				console.log("completed>>");
			}
		});
	},
	currentGPSInfo : function(currentCity, currentState, currentZipCode,
			currentCountry) {
		// status 0 for no ps, 1 for yes gps
		window.localStorage.setItem("currentCity", currentCity);
		window.localStorage.setItem("currentState", currentState);
		window.localStorage.setItem("currentZipCode", currentState);
		window.localStorage.setItem("currentCountry", currentCountry);

	},
	currentGPSInfo1 : function(currentLat, currentLong) {
		window.localStorage.setItem("currentLat", currentLat);
		window.localStorage.setItem("currentLong", currentLong);
	},
	userFilter:function(people_who_contact_you,want_chat,pic_trading,willing_meet,willing_hook_up,want_relation,want_men,want_women,min_age,max_age,have_pick,online_people){
		window.localStorage.setItem("people_who_contact_you", people_who_contact_you);
		window.localStorage.setItem("want_chat", want_chat);
		window.localStorage.setItem("pic_trading", pic_trading);
		window.localStorage.setItem("willing_meet", willing_meet);
		window.localStorage.setItem("willing_hook_up", willing_hook_up);
		window.localStorage.setItem("want_relation", want_relation);
		window.localStorage.setItem("want_men", want_men);
		window.localStorage.setItem("want_women", want_women);
		window.localStorage.setItem("min_age", min_age);
		window.localStorage.setItem("max_age", max_age);
		window.localStorage.setItem("have_pick", have_pick);
		window.localStorage.setItem("online_people", online_people);
	},
	loadFilterResult:function(){
		var data="";

		if(window.localStorage.getItem("people_who_contact_you") !=null || window.localStorage.getItem("people_who_contact_you") !=""){
			data=data+"p_who_con_you="+window.localStorage.getItem("people_who_contact_you");	
			}
		if(window.localStorage.getItem("want_chat")){
			data=data+"&want_chat="+window.localStorage.getItem("want_chat");
		}
		if(window.localStorage.getItem("pic_trading")){
			data=data+"&pic_trading="+window.localStorage.getItem("pic_trading");
		}
		if(window.localStorage.getItem("willing_meet")){
			data=data+"&willing_meet="+window.localStorage.getItem("willing_meet");
		}
		if(window.localStorage.getItem("willing_hook_up")){
			data=data+"&willing_hook_up="+window.localStorage.getItem("willing_hook_up");	
		}
		if(window.localStorage.getItem("want_relation")){
			data=data+"&want_relation="+window.localStorage.getItem("want_relation");
		}
		if(window.localStorage.getItem("want_men")){
			data=data+"&want_men="+window.localStorage.getItem("want_men");
		}
		if(window.localStorage.getItem("want_women")){
			data=data+"&want_women="+window.localStorage.getItem("want_women");
		}
		if(window.localStorage.getItem("min_age")){
			data=data+"&min_age="+window.localStorage.getItem("min_age");
		}
		if(window.localStorage.getItem("max_age")){
			data=data+"&max_age="+window.localStorage.getItem("max_age");
		}
		if(window.localStorage.getItem("have_pick")){
			data=data+"&have_pick="+window.localStorage.getItem("have_pick");
		}
		if(window.localStorage.getItem("online_people")){
			data=data+"&online_people="+window.localStorage.getItem("online_people");
		}
		return data;	
		},
	
		
};
